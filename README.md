Documentation
===
    Documentation about dataglen codebase can be found here:
    http://dataglen.com/

Steps to setup KITE
===
   Latest version: 1.0.0

1) Clone the adwyze assignment repo:

        git clone https://birendra7654@bitbucket.org/birendra7654/data_glen.git

2) Create a virtual environment and install requirements

        virtualenv dataglen_env
        source dataglen_env/bin/activate
        cd data_glen
        pip install -r requirements.txt

4) Run the server

        python manage.py runserver

5) Create superuser
        django-admin createsuperuser

5) Follow this URl in Browser to see all API:
        http://127.0.0.1:8000/docs/
