from rest_framework import serializers
from movie_review.models import Movie, TYPE_MOVIE_CHOICES
from movie_review.models import MovieReview
from django.contrib.auth.models import User


class MovieSerializer(serializers.Serializer):
    movie_id = serializers.IntegerField(read_only=True)
    movie_name = serializers.CharField(required=True, allow_null=False, allow_blank=False, max_length=50)
    year = serializers.IntegerField(required=False, allow_null=False)
    wiki_link = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=60)
    average_rating = serializers.FloatField(allow_null=True, default=0.0)
    total_review_by_people = serializers.IntegerField(allow_null=True, default=0)
    movie_release_date = serializers.DateTimeField(required=False, allow_null=True)
    description = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=200)
    director = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=30)
    writer = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=30)
    stars = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=200)
    type_of_movie = serializers.ChoiceField(choices=TYPE_MOVIE_CHOICES, default='DR')

    def create(self, validated_data):
        """
        Create and return a new `Movie` instance, given the validated data.
        """
        return Movie.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Movie` instance, given the validated data.
        """
        instance.total_review_by_people = validated_data.get('total_review_by_people', instance.total_review_by_people)
        instance.average_rating = validated_data.get('average_rating', instance.average_rating)
        instance.movie_name = validated_data.get('movie_name', instance.movie_name)
        instance.save()
        return instance


class MovieReviewSerializer(serializers.Serializer):

    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    movie_id = serializers.PrimaryKeyRelatedField(queryset=Movie.objects.all())
    rating = serializers.IntegerField(allow_null=True, default=0)
    ip_address = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=40)
    comment = serializers.CharField(required=False, allow_blank=True, allow_null=True, max_length=1000)
    published_date = serializers.DateTimeField(required=False, allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `Movie` instance, given the validated data.
        """
        return MovieReview.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Movie` instance, given the validated data.
        """
        instance.save()
        return instance
