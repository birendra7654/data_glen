import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from movie_review.serializers import MovieSerializer, MovieReviewSerializer
from rest_framework import status
from movie_review.models import Movie, MovieReview
from django.contrib.auth.models import User
# from rest_framework.authentication import TokenAuthentication
# from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
import json
import traceback
import sys
from decimal import Decimal

log = logging.getLogger(__name__)


class MovieList(APIView):

    # authentication_classes = (TokenAuthentication, )
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        The API Endpoint to retrieve a list of Movie by name. It allows the following
        parameters to filter by.
        movie_name -- name of the movie.
        """
        query_params = request.query_params
        try:
            res = Movie.objects.filter(movie_name__icontains=query_params["movie_name"])
            results = []
            for item in res:
                print item
                serializer = MovieSerializer(item)
                results.append(serializer.data)
            return Response({"movies": results}, status=status.HTTP_200_OK)
        except Movie.DoesNotExist:
            return Response({"message": "No such movie exist in our database"},
                            status=status.HTTP_200_OK)

    def post(self, request, format=None):
        """
        The API Endpoint to insert a new Movie item.

        Please insert json e.g..

        {"movie_name": "GOT", "description": "", "writer": "", "director": "", ...}

        Note: Initial value of average_rating and total_review_by_people will be 0.0
              and 0 respectively when new movie is being added
        ---
        response_serializer: MovieSerializer
        parameters:
        - name: body
          pytype: MovieSerializer
          paramType: body
        """

        cdr = request.data
        if "year" not in cdr:
            year = timezone.now().year
        else:
            year = cdr['year']
        try:
            Movie.objects.get(movie_name__iexact=cdr["movie_name"], year=year)
            return Response({"message": "Movie name is already exist in our database"},
                            status=status.HTTP_201_CREATED)
        except Movie.DoesNotExist:
            # exception occurs that means there is no such movie in our database
            ser = MovieSerializer(data=cdr)
            if ser.is_valid():
                ser.save()
                # log.debug(ser.data)
                return Response(ser.data, status=status.HTTP_201_CREATED)

            log.error("Error while POSTing to new ovie instance API: %s" % ser.errors)
            log.error("The data sent was: %s" % cdr)
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        except Movie.MultipleObjectsReturned:
            return Response({"message": "there is already more than one entry of this movie"},
                            status=status.HTTP_400_BAD_REQUEST)


class MovieReviewList(APIView):

    # authentication_classes = (TokenAuthentication, )
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        The API Endpoint to retrieve a list movie with movie review and user's comment and their review
        bu movie's id.
        It allows the following parameters to filter by.
        movie_id -- name of the movie.
        """
        query_params = request.query_params
        try:
            res = MovieReview.objects.filter(movie_id=query_params["movie_id"])
            movie_review = Movie.objects.get(movie_id=query_params["movie_id"])
            serializer = MovieSerializer(movie_review)
            result = {}
            result['movie_review'] = serializer.data
            print result
            data = []
            if res:
                for item in res:
                    temp = {}
                    temp['rating'] = item.rating
                    temp['comment'] = item.comment
                    print item.user_id.username
                    temp['username'] = item.user_id.username
                    # serializer = MovieReviewSerializer(res)
                    data.append(temp)
                result['user_review'] = data

            if result:
                return Response(result, status=status.HTTP_200_OK)
            else:
                return Response({"message": "No data"}, status=status.HTTP_204_NO_CONTENT)
        except Movie.DoesNotExist:
            return Response({"message": "No such movie exist in our database"},
                            status=status.HTTP_200_OK)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            return Response({"message": "Some internal problem %s" % e},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, format=None):
        """
        The API Endpoint to insert an user review for each movie.
        It also calculate rating on basis on previous average rating and number of people reviews
        Please insert json e.g..

        {"user_id": "dataglen", "movie_id": 4, "rating": 2, "comment": "kjakha ajhajah"}

        ---
        response_serializer: MovieReviewSerializer
        parameters:
        - name: body
          pytype: MovieReviewSerializer
          paramType: body
        """

        cdr = json.loads(request.body)
        log.debug('POST Request: movie users review with body=%s' % cdr)
        if cdr['rating'] < 1 and cdr['rating'] > 5:
            return Response({"message": "rating can not be more than 5 or less than 1"},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            res = Movie.objects.get(movie_id=cdr["movie_id"])
        except Movie.DoesNotExist:
            # exception occurs that means there is no such movie in our database
            return Response({"message": "Movie name does not exist in our database"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)
        try:
            user = User.objects.get(username=cdr['user_id'])
            cdr['user_id'] = user.id
        except User.DoesNotExist:
            return Response({"message": "user-id does not exist in our database"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)

        try:
            MovieReview.objects.get(user_id=user.id, movie_id=res)
            return Response({"message": "Users already reviewd this movie"},
                            status=status.HTTP_409_CONFLICT)
        except MovieReview.DoesNotExist:
            total_ratings = res.average_rating * res.total_review_by_people
            # incrementing total_review_by_people by 1
            res.total_review_by_people += 1
            # adding current rating andtotal rating
            total_ratings = total_ratings + cdr['rating']
            # calculating total rating given by all people
            res.average_rating = total_ratings / Decimal(res.total_review_by_people)
            update_movie_rating = {}
            update_movie_rating['average_rating'] = res.average_rating
            update_movie_rating['movie_name'] = res.movie_name
            update_movie_rating['total_review_by_people'] = res.total_review_by_people
            movie_ser = MovieSerializer(res, data=update_movie_rating, partial=True)
            if movie_ser.is_valid():
                ser = MovieReviewSerializer(data=cdr)
                if ser.is_valid():
                    ser.save()
                    movie_ser.save()
                    log.debug(ser.data)
                    return Response(ser.data, status=status.HTTP_201_CREATED)

                log.error("Error while POSTing to Movie review API: %s" % ser.errors)
                log.error("The data sent was: %s" % cdr)
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
            log.error("Error while POSTing to Movie API: %s" % movie_ser.errors)
            log.error("The CDR sent was: %s" % cdr)
            return Response(movie_ser.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomerActivityMovieReview(APIView):

    # authentication_classes = (TokenAuthentication, )
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        The API Endpoint to retrieve a list of user's activity. It allows the following
        parameters to filter by.
        user_id -- user_id of customer.
        """
        query_params = request.query_params
        try:
            res = MovieReview.objects.filter(user_id=query_params["user_id"])
            data = []
            result = {}
            if res:
                for item in res:
                    temp = {}
                    temp['rating'] = item.rating
                    temp['comment'] = item.comment
                    temp['movie_name'] = item.movie_id.movie_name
                    temp['published_date'] = item.published_date.strftime("%Y-%m-%d %H:%M:%S")
                    # serializer = MovieReviewSerializer(res)
                    data.append(temp)
                result['activity_log'] = data
                return Response(result, status=status.HTTP_200_OK)
            else:
                return Response({"message": "No data"}, status=status.HTTP_204_NO_CONTENT)
        except Movie.DoesNotExist:
            return Response({"message": "No such movie exist in our database"},
                            status=status.HTTP_200_OK)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            return Response({"message": "Some internal problem %s" % e},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
