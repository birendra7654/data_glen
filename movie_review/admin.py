from django.contrib import admin
from .models import Movie, MovieReview


class MovieAdmin(admin.ModelAdmin):
    fields = [
        'movie_name', 'wiki_link', 'year',
        'movie_release_date', 'description',
        'director', 'writer', 'stars',
        'type_of_movie']

    list_display = (
        'movie_name', 'wiki_link', 'year',
        'total_review_by_people', 'average_rating',
        'movie_release_date', 'description',
        'director', 'writer', 'stars',
        'type_of_movie')

    search_fields = ('movie_name', 'director', 'type_of_movie', 'writer', 'year')


class MovieReviewAdmin(admin.ModelAdmin):

    list_display = ('ip_address', 'comment', 'rating', 'user_id', 'movie_id')

    search_fields = ('ip_address', 'comment', 'rating', 'user__user_id', 'movie__movie_id')

# Register your models here.


admin.site.register(Movie, MovieAdmin)
admin.site.register(MovieReview, MovieReviewAdmin)
