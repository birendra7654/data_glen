from django.conf.urls import url
from v1_0 import views
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    url(r'^movie/$', views.MovieList.as_view()),
    url(r'^movie_review/$', views.MovieReviewList.as_view()),
    url(r'^user_activity/$', views.CustomerActivityMovieReview.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
