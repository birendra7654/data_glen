from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import logging
from django.core.validators import MaxValueValidator, MinValueValidator

# Initialize
logger = logging.getLogger(__name__)

# Create your models here.


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


TYPE_MOVIE_CHOICES = (
    ('DR', 'Drama'),
    ('CO', 'Comedy'),
    ('AC', 'Action'),
    ('HO', 'Horror'),
    ('SU', 'Suspense'),
    ('DO', 'Document'),
)


class Movie(models.Model):
    """
    Instance of a Movie.

    Admin can add new movie, user can give rating.

    On basis of user's rating, find average rating
    """
    movie_id = models.AutoField(_('movie code'), primary_key=True)
    movie_name = models.CharField(_('movie name'), max_length=50, blank=False, null=False)
    wiki_link = models.CharField(_('Wiki link of the movie'), max_length=60, blank=True, null=True)
    average_rating = models.DecimalField(_('Average rating'), default=0.0, max_digits=3,
                                         decimal_places=2)
    total_review_by_people = models.IntegerField(_('number of people reviewed'), default=0)
    year = models.IntegerField(_('movie released in which year'))
    movie_release_date = models.DateTimeField(_('release date'), default=timezone.now)
    description = models.CharField(_('description about movie'), max_length=200, blank=True, null=True)
    director = models.CharField(_('director of this movie'), max_length=30, blank=True, null=True)
    writer = models.CharField(_('writer of this movie'), max_length=30, blank=True, null=True)
    stars = models.CharField(_('comma separated movie stars'), max_length=200, blank=True, null=True)
    type_of_movie = models.CharField(max_length=2, choices=TYPE_MOVIE_CHOICES,
                                     default='DR')

    class Meta:
        ordering = ["movie_name"]

    def save(self, *args, **kwargs):
        """ override save to also create other model objects """
        logger.debug("Saving a new movie object")
        if not self.year:
            self.year = self.movie_release_date.year
        # Call the "real" save() method.
        super(Movie, self).save(*args, **kwargs)
        return self

    def __str__(self):
        return self.movie_name


class MovieReview(models.Model):
    user_id = models.ForeignKey(User, related_name='user_tracks', on_delete=models.CASCADE)
    movie_id = models.ForeignKey(Movie, related_name='movie_tracks', on_delete=models.CASCADE)
    ip_address = models.CharField(_('client ip4/ip6 address '), max_length=40, blank=True, null=True)
    comment = models.CharField(_('comment if any'), max_length=1000, blank=True, null=True)
    rating = models.IntegerField(_('movie rating per customer'),
                                 default=1,
                                 validators=[MaxValueValidator(5), MinValueValidator(1)],
                                 blank=False, null=False)
    published_date = models.DateTimeField(_('publish date'), default=timezone.now)

    def save(self, *args, **kwargs):
        """ override save to also create other model objects """
        logger.debug("Saving a new movieReview object")
        # Call the "real" save() method.
        super(MovieReview, self).save(*args, **kwargs)
        return self
