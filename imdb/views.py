from django.http import JsonResponse
import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from django import db
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q

from django.shortcuts import render

log = logging.getLogger(__name__)


class UserSignUP(APIView):
    def post(self, request, format=None):
        """
        The API Endpoint to insert new User signup information.
        {"username": "dataglen", "first_name": "data", "last_name": "glen", "email": "dataglen@gmail.com"}
        ---
        parameters:
        - name: body
          paramType: body
        """
        record = request.data
        try:
            # User.objects.create_user(**record)
            User.objects.get(Q(email=record['email']) | Q(username=record['username']))
            return Response({"message": "Email id/Username is already exist", "error": True})
        except User.DoesNotExist:
            try:
                User.objects.create_user(username=record['username'],
                                         first_name=record['first_name'],
                                         last_name=record['last_name'],
                                         password=record['password'],
                                         email=record['email'])
                request.session['username'] = record['username']
                return Response({"message": "User created successfully",
                                 "status": status.HTTP_201_CREATED})
            except db.IntegrityError:
                # Render a page to show succesfull signup
                return Response({"message": "Username is already exist", "error": True})
            except:
                return Response({"message": "Internal Server Error or DB error", "error": True})

#
# def login_api(request):
#     """The API Endpoint for login information.
#
#     ---
#     parameters:
#     - name: body
#       paramType: body
#     """
#     if request.method == 'POST':
#         data = request.POST
#         print data
#         if 'username' in data.keys() and 'password' in data.keys():
#             user = authenticate(username=data['username'], password=data['password'])
#             if user is not None:
#                 if user.is_active:
#                     # render a page for successfully logn page
#                     login(request, user)
#                     request.session['user_id'] = user.user_id
#                     # return redirect('/api/profile/')
#                     # return redirect("home-page")
#                     return JsonResponse({"message": "Login successfully", 'error': False}, status=200)
#                 else:
#                     # render a page for disable user account
#                     return JsonResponse({"message": "Your account is disable for time being, contact admin",
#                                         "error": True}, status=420)
#             else:
#                 # render a page for invalig login attempt
#                 try:
#                     User.objects.get(email=data['email'])
#                 except User.DoesNotExist:
#                     # Ajax query to suggest email does not exist
#                     return JsonResponse({"message": "Email does not exist. Try to signup first", "error": True},
#                                         status=420)
#                 # Ajax query to suggest to Invalid password
#                 return JsonResponse({"message": "Invalid password", "error": True}, status=420)
#         else:
#             # Validation from front end side
#             return JsonResponse({"message": "Username/password is missing"}, status=420)
#     else:
#         return JsonResponse({"message": "Not a valid URl for login API"}, status=420)
#
#
# def logout_api(request):
#     """GET API for logout."""
#     if request.method == 'GET':
#         logout(request)
#         # Ajax query to return home page
#         # return redirect("")
#         return JsonResponse({"message": "User successfully logged out"})
#     else:
#         return JsonResponse({"message": "Not a valid URl for logout API"})
#
#
# def signup_page(request):
#     """The view for the landing page."""
#     if request.method == 'GET':
#         if request.user.is_authenticated():
#             # render direct dashborad/ profile page
#             return JsonResponse({"message": "Already loggeg in, don't need to signup", "error": False})
#         return render(request, 'user_login_signup/signup.html')
#     else:
#         return JsonResponse({"message": "Not a valid URl for rendering signup page"})
#
#
# def login_page(request):
#     """The view for the landing login page."""
#     if request.method == 'GET':
#         print request.user.is_authenticated()
#         if request.user.is_authenticated():
#             return JsonResponse({"message": "Already loggeg in", "error": False})
#         return render(request, 'user_login_signup/login.html')
#     else:
#         return JsonResponse({"message": "Not a valid URl for logout API"})
