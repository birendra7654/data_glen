"""imdb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from .views import UserSignUP
# from .views import signup_api, login_api, logout_api
# from .views import signup_page, login_page

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^signup/', UserSignUP.as_view()),


    # url(r'^api/user_signup/$', signup_api, name="new-user-signup-api"),
    # url(r'^api/user_login/$', login_api, name="each-user-login-api"),
    # url(r'^logout/$', logout_api, name="each-user-logout-api"),
    #
    # url(r'^signup/$', signup_page),
    # url(r'^$', login_page),

    url(r'^v1.0/data_glen/', include('movie_review.urls')),
]
